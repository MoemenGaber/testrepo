function isMobile() {
    if ((window.innerWidth <= 700)) {
        return true;
    }
    else {
        return false;
    }

}


document.addEventListener('DOMContentLoaded', function () {
    Feld.init();
});

var Feld = {
    testFunction: function () {
        console.log('kodo.house');
    },

    settings: {
        minDesktopWidth: 1025
    },

    vars: {
        isMobile: this.isMobile()
    },

    elements: {
        $window: $(window)
    },

    cursor: {
        $cursorEl: $('.js-cursor'),
        cursorTimeout: null,

        init: function () {
            $(window).on('mousemove', this.followCursor)
            this.viewCursor();
        },

        followCursor: function (e) {
            var posX = e.clientX;
            var posY = e.clientY;
            // Feld.cursor.$cursorEl.css('transform', 'translate3d(' + posX + 'px, ' + posY + 'px, 0)');
            Feld.cursor.$cursorEl.css({
                'top': posY,
                'left': posX
            })
        },

    },

    init: function () {
        !Feld.vars.isMobile && Feld.cursor.init();
    }
}